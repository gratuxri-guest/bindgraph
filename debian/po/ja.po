#
#    Translators, if you are not familiar with the PO format, gettext
#    documentation is worth reading, especially sections dedicated to
#    this format, e.g. by running:
#         info -n '(gettext)PO Files'
#         info -n '(gettext)Header Entry'
#
#    Some information specific to po-debconf are available at
#            /usr/share/doc/po-debconf/README-trans
#         or http://www.debian.org/intl/l10n/po-debconf/README-trans
#
#    Developers do not need to manually edit POT or PO files.
#
#
msgid ""
msgstr ""
"Project-Id-Version: bindgraph 0.2a-1\n"
"Report-Msgid-Bugs-To: bindgraph@packages.debian.org\n"
"POT-Creation-Date: 2008-05-10 14:39+0200\n"
"PO-Revision-Date: 2007-03-08 01:30+0900\n"
"Last-Translator: Hideki Yamane (Debian-JP) <henrich@debian.or.jp>\n"
"Language-Team: Japanese <debian-japanese@lists.debian.org>\n"
"Language: ja\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: boolean
#. Description
#: ../templates:1001
msgid "Should bindgraph start on boot?"
msgstr "システム起動時に BindGraph を開始しますか?"

#. Type: boolean
#. Description
#: ../templates:1001
msgid ""
"Bindgraph can start on boot time as a daemon. Then it will monitor your BIND "
"logfile for changes. This is recommended."
msgstr ""
"bindGraph はシステム起動時にデーモンとして起動できます。この設定をすると、"
"bindgraph は BIND のログファイルの変更を監視します。お勧めです。"

#. Type: boolean
#. Description
#: ../templates:1001
msgid "The other method is to call bindgraph.pl by hand with the -l parameter."
msgstr ""
"他の方法としては、-l パラメータ付きで bindgraph.pl を手動で呼び出すというのが"
"あります。"

#. Type: string
#. Description
#: ../templates:2001
msgid "Bindgraph log file:"
msgstr "bindgraph のログファイル:"

#. Type: string
#. Description
#: ../templates:2001
msgid ""
"Please specify the name of the log file from which data should be pulled to "
"create the databases for bindgraph. If unsure, leave the default value."
msgstr ""
"bindgraph 用データベースを作成するために使うログファイル名を入力してくださ"
"い。わからない場合は、デフォルトのままにしておいてください。"

#. Type: boolean
#. Description
#: ../templates:3001
msgid "Remove RRD files on purge?"
msgstr "purge する際に RRD ファイルを削除しますか?"

#. Type: boolean
#. Description
#: ../templates:3001
msgid ""
"Bindgraph keeps its database files under /var/lib/bindgraph. Please choose "
"whether this directory should be removed completely on purge."
msgstr ""
"bindGraph は /var/lib/bindgraph の下にデータベースを保持しています。このディ"
"レクトリを purge の際に完全に削除するかどうかを選んでください。"

#~ msgid "BIND9 logging required"
#~ msgstr "BIND9 のログ機能 (logging) が必要です"

#~ msgid "Bindgraph needs BIND logging to be active in order to work."
#~ msgstr ""
#~ "bindGraph がきちんと動くには BIND のログ機能 (logging) が動作している必要"
#~ "があります。"

#~ msgid ""
#~ "The required configuration in 'named.conf' is explained in /usr/share/doc/"
#~ "bindgraph/README."
#~ msgstr ""
#~ "'named.conf' 中での必要な設定は、/usr/share/doc/bindgraph/README で説明さ"
#~ "れています。"

#~ msgid "Which logfile should be used by BindGraph?"
#~ msgstr "BindGraph はどのログファイルを使いますか?"
