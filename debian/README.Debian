BindGraph for Debian
====================

BindGraph gathers statistics about a BIND9 server's 
activity and presents them in graphical form on a web page. It does
this using the excellent RRDTOOL 
(http://people.ee.ethz.ch/~oetiker/webtools/rrdtool/).


Please note that bind9 must be configured to log queries to a file before
bindgraph can be used.

An example configuration file snippet is provided below:

----
logging {
  channel "querylog" { file "/var/log/bind/queries.log"; print-time yes; };
  category queries { querylog; };
  category lame-servers { null; };
};
----

This will set bind9 to log to "/var/log/bind/queries.log";
Bindgraph must be set up to read from that file. This configuration is stored
in the /etc/default/bindgraph file, along with the file format (bind93 for
bind9 versions 9.3 or higher, including 9.4)


This Debian package installs itself, optionally, as a daemon that
will constantly gather the statistics it needs. You really need to turn
this option on for it to be of any use. The statistics themselves are
served up by a web CGI program, /usr/lib/cgi-bin/bindgraph.cgi. With any
normal Debian web server install, this should be accessible as
http://localhost/cgi-bin/bindgraph.cgi


Upstream (namely Marco D'Itri) is not responsible for *any* bugs introduced
in this software -- please do not contact them regarding this matter and do
notify me so that they can be solved. Thanks.

 -- Jose Luis Tallon <jltallon@adv-solutions.net>  Sat, 29 May 2004 22:13:08 +0200
